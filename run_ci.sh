#!/usr/bin/env bash

# TODO: run with valgrind once gfortran memory leaks have been fixed
# i.e.  --runner "valgrind --leak-check=full --error-exitcode=1"

set -e

compiler="${1:-gfortran}"
flags="${2:--g -Wall -Wextra -Werror -pedantic -std=f2018 -Wimplicit-interface -fcheck=all -fbacktrace -finit-real=snan -ffpe-trap=invalid,zero,overflow,underflow,denormal}"

fpm test --compiler "${compiler}" --flag "${flags}" --target unit_test
fpm test --compiler "${compiler}" --flag "${flags}" --target round_trip
./run_io_tests.sh "${1}" "${2}"
